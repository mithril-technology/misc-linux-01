Red Hat Linux notes

VM stuff to do after install
$ sudo dnf install open-vm* NetworkManager-tui cockpit-*
$ sudo vi /etc/bashrc
 Add these lines to bottom:
# MFryman added
set -o vi
HISTSIZE=50000
HISTSIZE=50000
$ sudo dnf update

Setting up RHEL8 router between subnets
Install rhel8/fedora34/CentOS/RockyLinux on router system

Example network info
This example is using 2 VMware VMs running Fedora v34 Server 
Domain:  aa.lan    (instead of example.com or whatever)
Router system has 2 NICs
External - static ip set to 192.168.3.5/22 GW 192.168.0.1   (using VMware bridged vNIC)
ISP router at 192.168.0.1 for this example
Internal - status ip set to 172.16.16.5/24    (using VMware host-only vNIC)
No gateway setup, or set it to our own static IP address 172.16.16.5
guest /test system with 1 NIC
Can use DHCP after we setup DNSmasq, but until then set static IP address
172.16.16.21 GW 172.16.16.5     (using VMware host-only vNIC)

On router machine turn on ip packet forwarding
$ sudo sysctl -w net.ipv4.ip_forward=1
$ sudo sysctl -p
check it with this
$ sudo sysctl net.ipv4.ip_forward
net.ipv4.ip_forward = 1


If needed for access the first time, on a guest/test system inside the 172 network add this static route
$ sudo ip route add 192.168.0.0/22 via 172.16.16.5

At this point you can ping from 172.16.16.21 (test system) to 192.168.3.5 on router box, but you can’t ping google.com or ping anything else on internet or 192.168.3.0/24 network.  Most likely this is because of firewall running by default on router system.
You can verify this on router system:
$ sudo systemctl status firewalld.service
You’ll see that it is probably “active” i.e. running
But even if you stop the firewall, nothing changes in that the guest/test system can’t ping or DNS resolve anything outside the 172.16.16.0/24 network.

IMPORTANT:  Back on the router system, do this to turn on masquerade for the firewall:
$ sudo firewall-cmd --add-masquerade --permanent
$ sudo firewall-cmd --reload

Now on the guest/test system change the DNS to something like CloudFlare’s public DNS server 1.1.1.1 or Google’s 8.8.8.8.

to see your connections and profiles
$ sudo nmcli con show
pick the connection profile you need, such as “ens224-static172”

$ sudo nmcli con mod ens224-static172 ipv4.dns 1.1.1.1

you can see what you have set now for everything with this:
$ sudo nmcli con show ens224-static172

# Now if you test, you should be able to get on the guest/test system and ping these places:
$ ping 172.16.16.5
$ ping 192.168.3.5
$ ping 192.168.3.xxx     (something else on your external network, just to test it out)
$ ping 1.1.1.1       (CloudFlare’s public free DNS server)
$ ping google.com

Checkpoint #1: So Far So Good !!
Firewall commands now….
For now all of this work below takes place on the router system.

$ sudo firewall-cmd --get-zones
# you’ll see a bunch of zones that were created by default.

$ sudo firewall-cmd --list-all

on Fedora Server installs, the default zone interfaces get assigned to FedoraServer
It has these settings:
$ sudo firewall-cmd --list-all
FedoraServer (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens160 ens224
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:



This isn’t as clear as we’d like it to be for this as a router, so let’s move the interfaces around.
in our case the “external” and “internal”  zones already defined make sense, so let’s use those.
external here will be our 192.168.3.0/24 network, ens160
internal here will be our 172.16.16.0/24 network, ens224
first we make “external” the new default zone
$ sudo firewall-cmd --set-default-zone=external
$ sudo firewall-cmd --permanent --zone=external --add-interface=ens160
$ sudo firewall-cmd --permanent --zone=internal --add-interface=ens224
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --add-masquerade --permanent --zone=external
	Probably will fail as it is already set from earlier steps, but just in case, you know
$ sudo firewall-cmd --reload

****  IMPORTANT ******
###  To be able to actually access ssh, cockpit, etc. inside the 172.16.16.0/24 network you need to do these steps
root@ns11 ~]# firewall-cmd --permanent --set-target=ACCEPT --zone=internal
success
[root@ns11 ~]# firewall-cmd --reload

Checkpoint #2: Still Good !!

# now we need to check and add some services allowed, perhaps
$ sudo firewall-cmd --get-default
$ sudo firewall-cmd --get-active-zones
$ sudo firewall-cmd --list-all --zone=internal
internal (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens224
  sources:
  services: dhcpv6-client mdns samba-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
$ sudo firewall-cmd --list-all --zone=external
external (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens160
  sources:
  services: ssh
  ports:
  protocols:
  forward: no
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:



# now let’s add some more services, as both external and internal by default don’t have all the services we want allowed/accepted.
$ sudo firewall-cmd --permanent --zone=external --add-service={cockpit,dhcpv6-client}
$ sudo firewall-cmd --permanent --zone=internal --add-service=cockpit
$ sudo firewall-cmd --reload

Now the zones look like this
$ sudo firewall-cmd --zone=external --list-all
external (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens160
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
$ sudo firewall-cmd --zone=internal --list-all
internal (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens224
  sources:
  services: cockpit dhcpv6-client mdns samba-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

Checkpoint #3: Still Good !!
DNSmasq install & setup

IntGuest1 VM MAC address for use later in assigned DHCP address:  00:0C:29:B6:48:11

# install the DNSmasq software.  On many modern distros in 2020 and later, dnsmasq seems to 
# already be install by default now.  Like Ubuntu 20.04, RHEL8, Fedora 33/34, etc. but not Rocky Linux.
$ sudo dnf install dnsmasq

# look at /etc/dnsmasq.conf .   You can usually leave all this alone.  The main thing
# is we want to make sure it will look for more config files in /etc/dnsmasq.d/  folder.
# This is what we found in ours:
>>	# Include all files in /etc/dnsmasq.d except RPM backup files
>>	conf-dir=/etc/dnsmasq.d,.rpmnew,.rpmsave,.rpmorig
tftp folder
# make our TFTPboot folder and then set correct SElinux context
$ sudo mkdir /var/lib/tftpboot
$ sudo restorecon -Rv /var/lib/tftpboot

# add DNSmasq services to the firewall
$ sudo firewall-cmd --permanent --zone=internal --add-service={dns,dhcp,tftp}
$ sudo firewall-cmd --reload
success
$ sudo firewall-cmd --zone=internal --list-all
internal (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens224
  sources:
  services: cockpit dhcp dhcpv6-client dns mdns samba-client ssh tftp
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:



# test the dnsmasq config, then restart and verify the service
$ sudo dnsmasq --test
$ sudo systemctl restart dnsmasq.service
$ sudo systemctl status dnsmasq.service

Or you could do this for quick shorter method:
$ sudo systemctl restart dnsmasq && sudo systemctl is-active dnsmasq

Checkpoint #4: Still Good !!
Now time to put our own dnsmasq.conf type file in there
$ sudo vi /etc/dnsmasq.d/lab-dnsmasq.conf

Make sure you have a DHCP entry for your guest/test system with its 
MAC address correct, to test that functionality. Like this:

dhcp-host=a8:a1:59:83:a7:70,172.16.16.21,asrocka1
host-record=asrocka1.lab.mithril.lan,172.16.16.21

And in our tests, we had to make sure that the auth-server and auth-zone were commented out, or else DNS for internal hosts wasn’t working!
	# auth-server=xxxxxxxx
	# auth-zone=xxxxxxxx

$ sudo systemctl restart dnsmasq && sudo systemctl is-active dnsmasq

Now you should change the internal/guest system and test that DHCP by MAC address
is working correctly.
Checkpoint #5: Still Good !!
After all the steps above, our router system and our internal host system were all working correctly
IP addresses set or obtained by DHCP
IP forwarding working
Firewall set correctly
DNS for internal and external hosts is working okay
So it looks good!!

Additional Notes
DNSMASQ not starting at boot
I had trouble with some systems not successfully starting, or failing to start, dnsmasq.service at boot, i.e. after reboots.
Upon checking the logs (“journalctl | grep dnsmasq | grep FAIL”) it looked like it was failing to start at boot time because the interface(s) weren’t ready yet.
This was on Rocky Linux v8, but could also be RHEL 8 or Fedora 34 (probably v30+).

I found this possible solution:
https://ask.fedoraproject.org/t/dnsmasq-fails-on-initial-startup-succeeds-on-restart-unknown-interface-or-failed-to-create-listening-socket/16893/3 
Which refers to this site:  https://src.fedoraproject.org/rpms/dnsmasq/blob/rawhide/f/dnsmasq.service#_6-7 

The first thing I tried was this.
Edit this file:
 	$ sudo vi /etc/systemd/system/multi-user.target.wants/dnsmasq.service 

            Mine (in Fedora 34) looked like this:
[Unit]
Description=DNS caching server.
After=network.target

[Service]
ExecStart=/usr/sbin/dnsmasq -k

[Install]
WantedBy=multi-user.target

	I put a semi-colon to comment out the After line in the [Unit] section, and added instead the line “After=network-online.target”
[Unit]
Description=DNS caching server.
;After=network.target
After=network-online.target

[Service]
ExecStart=/usr/sbin/dnsmasq -k

[Install]
WantedBy=multi-user.target

In my tests on a small 4x4 mini-pc using a crappy Celeron J3xxx series chip this worked.  The service started now after every reboot.
FTP server setup
Ftp server for OS installs
# set the SElinux context for your ftp folder for each OS folder you create.  Here’s an example for a folder to hold your centos8 OS files.
semanage fcontext -a -t public_content_t “/var/ftp/pub/centos8(/.*)?”
restorecon -Rv /var/ftp/pub/centos8

# and now you copy the centos8 kernel and initrd files to folder  /var/lib/tftpboot/centos8



Samples :: dnsmasq   
ASrockA1  example-lab.conf   file
Network Layout:
DellRouter:  192.168.1.1   172.16.16.1
ASrocka1:	172.16.16.21

######################################################################################
# Created by John Call, modified by Michael Fryman

### DNS STUFF ###
listen-address=127.0.0.1
listen-address=172.16.16.1
bind-interfaces
no-resolv
#server=192.168.0.7
#server=192.168.0.1
server=1.1.1.1
local=/lab.mithril.lan/  # answer queries for *.lab.mithril.lan from /etc/hosts, DHCP records, or this config file, but never forward upstream
expand-hosts                     # put "lab.mithril.lan" onto short names from /etc/hosts
#log-queries
#log-queries=extra

# DON'T USE auth-server= if you don't have a dual-NIC host
# where requests coming from the "outside" NIC need to be
# treated differently than requests from the "inside" NIC
#auth-server=lab.mithril.lan,enp4s0
#auth-soa=20210729,mfryman.redhat.com
#auth-zone=lab.mithril.lan
host-record=lab.mithril.lan,192.168.1.1
host-record=ns.lab.mithril.lan,192.168.1.1
host-record=dns.lab.mithril.lan,192.168.1.1

### DHCP + TFTP STUFF ###
enable-tftp
tftp-root=/var/lib/tftpboot                     # don't forget to `mkdir /var/lib/tftpboot`
dhcp-range=172.16.16.101,172.16.16.199
dhcp-option=option:router,172.16.16.1
#dhcp-option=option:ntp-server,10.11.160.238     # clock.corp.redhat.com
dhcp-boot=pxelinux/pxelinux.0                   # Allow BIOS PXE requests (this is discouraged, use UEFI PXE instead)
dhcp-match=set:efi-x86_64,option:client-arch,7  # Allow UEFI PXE requests (step 1)
dhcp-boot=tag:efi-x86_64,shimx64-redhat.efi     # Allow UEFI PXE requests (step 2)
domain=lab.mithril.lan                          # DHCP clients are given this as their domain name

########################################################################################
# ASrocka1 box
dhcp-host=a8:a1:59:83:a7:70,172.16.16.21,asrocka1
host-record=asrocka1.lab.mithril.lan,172.16.16.21

## reference material
# OCP-BLUE (IBM POWER8)
#cname=*.apps.ocp-blue.dota-lab.iad.redhat.com,ingress.ocp-blue.dota-lab.iad.redhat.com
#host-record=ingress.ocp-blue.dota-lab.iad.redhat.com,10.15.168.124
#host-record=api.ocp-blue.dota-lab.iad.redhat.com,10.15.168.125
#host-record=api-int.ocp-blue.dota-lab.iad.redhat.com,10.15.168.125
#host-record=ocp-blue-haproxy.dota-lab.iad.redhat.com,10.15.168.125
#dhcp-host=10.15.168.125,ocp-blue-haproxy.dota-lab.iad.redhat.com
#host-record=ocp-blue-1.dota-lab.iad.redhat.com,10.15.168.126
#dhcp-host=ae:56:cb:29:ad:02,10.15.168.126,ocp-blue-1
#host-record=ocp-blue-2.dota-lab.iad.redhat.com,10.15.168.127
#dhcp-host=ae:56:c3:4f:6e:02,10.15.168.127,ocp-blue-2
#host-record=ocp-blue-3.dota-lab.iad.redhat.com,10.15.168.128
#dhcp-host=ae:56:c5:6d:51:02,10.15.168.128,ocp-blue-3
#host-record=ocp-blue-bootstrap.dota-lab.iad.redhat.com,10.15.168.129
#dhcp-host=ae:56:cd:61:77:02,10.15.168.129,ocp-blue-bootstrap

# IBM POWER8 SERVERS
#dhcp-boot=tag:bootp,powerpc-ieee1275/core.elf
#dhcp-host=98:be:94:7e:25:9c,10.15.168.104,power-a-sp
#dhcp-host=98:be:94:4c:a9:46,10.15.168.105,power-b-sp
#dhcp-host=52:54:00:38:4a:0b,10.15.168.106,power-hmc
#host-record=power-hmc.dota-lab.iad.redhat.com,10.15.168.106
#host-record=power-a-vios1.dota-lab.iad.redhat.com,10.15.168.107
#host-record=power-a-vios2.dota-lab.iad.redhat.com,10.15.168.108
#host-record=power-b-vios1.dota-lab.iad.redhat.com,10.15.168.109

#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-0.jrj.dota-lab.iad.redhat.com.,2380,0,10
#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-1.jrj.dota-lab.iad.redhat.com.,2380,0,10
#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-2.jrj.dota-lab.iad.redhat.com.,2380,0,10


VM Test-AA-Router lab-aa-lan.conf

Network Layout:
Test-AA-Router:  192.168.3.5   172.16.16.5
Test-AA-IntGuest1    172.16.16.21
Domain:   aa.lan
######################################################################################
This files came from /etc/dnsmasq.d/172lab-dnsmasq.conf
######################################################################################

# Created by John Call, modified by Michael Fryman

### DNS STUFF ###
listen-address=127.0.0.1
listen-address=172.16.16.5
bind-interfaces
no-resolv
server=192.168.0.7
server=192.168.0.1
server=1.1.1.1
local=/aa.lan/  # answer queries for *.aa.lan from /etc/hosts, DHCP records, or this config file, but never forward upstream
expand-hosts                     # put "aa.lan" onto short names from /etc/hosts
#log-queries
#log-queries=extra
#domain-needed

### DON'T USE auth-server= if you don't have a dual-NIC host
### where requests coming from the "outside" NIC need to be
### treated differently than requests from the "inside" NIC
### so put your "internal" nic/interface for auth-server below
#auth-server=aa.lan,ens224
#auth-soa=20210729,michael.fryman.org
#auth-zone=aa.lan
host-record=router.aa.lan,172.16.16.5
host-record=aa.lan,192.168.3.5
host-record=ns.aa.lan,192.168.3.5
host-record=dns.aa.lan,192.168.3.5

### DHCP + TFTP STUFF ###
enable-tftp
tftp-root=/var/lib/tftpboot                     # don't forget to `mkdir /var/lib/tftpboot`
dhcp-range=172.16.16.101,172.16.16.199          # set your DHCP range here
dhcp-option=option:router,172.16.16.5           # set default router for your DHCP clients
#dhcp-option=option:ntp-server,10.11.160.238    # clock.corp.redhat.com
dhcp-boot=pxelinux/pxelinux.0                   # Allow BIOS PXE requests (this is discouraged, use UEFI PXE instead)
dhcp-match=set:efi-x86_64,option:client-arch,7  # Allow UEFI PXE requests (step 1)
dhcp-boot=tag:efi-x86_64,shimx64-redhat.efi     # Allow UEFI PXE requests (step 2)
domain=aa.lan                                   # DHCP clients are given this as their domain name

########################################################################################

host-record=router-int.aa.lan,172.16.16.5
host-record=bogus5.aa.lan,172.16.16.55
host-record=bogus6.aa.lan,172.16.16.66

# test VM
dhcp-host=00:0c:29:b6:48:11,172.16.16.21,intguest1
host-record=intguest1.aa.lan,172.16.16.21

#######################
## reference material follows, examples from John Call
#######################

# OCP-BLUE (IBM POWER8) for OpenShift Container Platform cluster "ocp-blue"
#cname=*.apps.ocp-blue.dota-lab.iad.redhat.com,ingress.ocp-blue.dota-lab.iad.redhat.com
#host-record=ingress.ocp-blue.dota-lab.iad.redhat.com,10.15.168.124
#host-record=api.ocp-blue.dota-lab.iad.redhat.com,10.15.168.125
#host-record=api-int.ocp-blue.dota-lab.iad.redhat.com,10.15.168.125
#host-record=ocp-blue-haproxy.dota-lab.iad.redhat.com,10.15.168.125
#dhcp-host=10.15.168.125,ocp-blue-haproxy.dota-lab.iad.redhat.com
#host-record=ocp-blue-1.dota-lab.iad.redhat.com,10.15.168.126
#dhcp-host=ae:56:cb:29:ad:02,10.15.168.126,ocp-blue-1
#host-record=ocp-blue-2.dota-lab.iad.redhat.com,10.15.168.127
#dhcp-host=ae:56:c3:4f:6e:02,10.15.168.127,ocp-blue-2
#host-record=ocp-blue-3.dota-lab.iad.redhat.com,10.15.168.128
#dhcp-host=ae:56:c5:6d:51:02,10.15.168.128,ocp-blue-3
#host-record=ocp-blue-bootstrap.dota-lab.iad.redhat.com,10.15.168.129
#dhcp-host=ae:56:cd:61:77:02,10.15.168.129,ocp-blue-bootstrap

# IBM POWER8 SERVERS
#dhcp-boot=tag:bootp,powerpc-ieee1275/core.elf
#dhcp-host=98:be:94:7e:25:9c,10.15.168.104,power-a-sp
#dhcp-host=98:be:94:4c:a9:46,10.15.168.105,power-b-sp
#dhcp-host=52:54:00:38:4a:0b,10.15.168.106,power-hmc
#host-record=power-hmc.dota-lab.iad.redhat.com,10.15.168.106
#host-record=power-a-vios1.dota-lab.iad.redhat.com,10.15.168.107
#host-record=power-a-vios2.dota-lab.iad.redhat.com,10.15.168.108
#host-record=power-b-vios1.dota-lab.iad.redhat.com,10.15.168.109

#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-0.jrj.dota-lab.iad.redhat.com.,2380,0,10
#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-1.jrj.dota-lab.iad.redhat.com.,2380,0,10
#srv-host=_etcd-server-ssl._tcp.jrj.dota-lab.iad.redhat.com,etcd-2.jrj.dota-lab.iad.redhat.com.,2380,0,10




RHV Red Hat Virtualization
Install the RHVH (RHV Host, i.e. the ESXi install)
Boot the RHVH install media.  We’re using v4.4.
Choose your language of choice


About to setup RHV Host-Engine
Please review the configuration. Once you click the 'Prepare VM' button, a local virtual machine will be started and used to prepare the management services and their data. This operation may take some time depending on your hardware.

VM
Engine FQDN:rhvm.lab.mithril.lan
MAC Address:00:16:3e:27:bd:12
Network Configuration:DHCP
Gateway Address:172.16.16.1
Root User SSH Access:yes
Number of Virtual CPUs:4
Memory Size (MiB):16384
Root User SSH Public Key:(None)
Add Lines to /etc/hosts:yes
Bridge Name:ovirtmgmt
Apply OpenSCAP profile:no

Engine
SMTP Server Name:localhost
SMTP Server Port Number:25
Sender E-Mail Address:root@localhost
Recipient E-Mail Addresses:root@localhost

First Step, SSH Keys
First thing you need to do is log into your host as root, generate new SSH key, then copy it to our selves and log in via SSH as ourselves to get added to the known_hosts file, it’s required!   Don’t put any pass phrase on the SSH key, leave it blank.
# ssh-keygen -t ed25519 -C "root@rhvh1.aa.lan"
# ssh-copy-id -i ~/.ssh/id_ed25519.pub root@rhvh1.aa.lan 
# ssh root@rhvh1.aa.lan  

You should add your intended disk block device that will be used for the Gluster storage to the 
blacklist {
       devnode "^sd[a-z]"
}


Wizard - walk thru
Go to Virtualization, then choose Hosted Engine.
Choose the Hyperconverged option to configure Gluster storage and oVirt hosted engine.
Click “START”
Choose “Run Gluster Wizard for Single Node”
Enter our FQDN hostname which in our test system was “rhvh1.aa.lan”  ,  click Next
John Call said we can just delete the “data” and “vmstore” volumes by clicking the trash can icon on the far right of each.
So just leave the “engine” volume entry.
Now for Gluster deployment, use this info
RAID Type :   JBOD
Change device name as appropriate.    In our test system it was /dev/nvme0n2
Change LV size to a bit less than the whole disk size.  For instance if you have 300G, put in maybe 260 or 280.
DO NOT enable the dedupe & compression
DO NOT enable “configure LV Cache”
Click NEXT.
Review the info displayed.  If it looks okay, click DEPLOY.






Preparing NFS Storage
Set up NFS shares on your file storage or remote server to serve as storage domains on Red Hat Enterprise Virtualization Host systems. After exporting the shares on the remote storage and configuring them in the Red Hat Virtualization Manager, the shares will be automatically imported on the Red Hat Virtualization hosts.
For information on setting up, configuring, mounting and exporting NFS, see Managing file systems for Red Hat Enterprise Linux 8.
Specific system user accounts and system user groups are required by Red Hat Virtualization so the Manager can store data in the storage domains represented by the exported directories. The following procedure sets the permissions for one directory. You must repeat the chown and chmod steps for all of the directories you intend to use as storage domains in Red Hat Virtualization.
Prerequisites
Install the NFS utils package.
# dnf install nfs-utils -y
To check the enabled versions:
# cat /proc/fs/nfsd/versions
Enable the following services:
# systemctl enable nfs-server
# systemctl enable rpcbind
Procedure
Create the group kvm:
# groupadd kvm -g 36
Create the user vdsm in the group kvm:
# useradd vdsm -u 36 -g kvm
Create the storage directory and modify the access rights.
# mkdir /storage
# chmod 0755 /storage
# chown 36:36 /storage/
Add the storage directory to /etc/exports with the relevant permissions.
# vi /etc/exports
# cat /etc/exports
 /storage *(rw)
Restart the following services:
# systemctl restart rpcbind
# systemctl restart nfs-server
To see which export are available for a specific IP address:
# exportfs
 /nfs_server/srv
               10.46.11.3/24
 /nfs_server       <world>
NOTE
If changes in /etc/exports have been made after starting the services, the exportfs -ra command can be used to reload the changes. After performing all the above stages, the exports directory should be ready and can be tested on a different host to check that it is usable.
Alternate NFS setup
SOURCE:  How to Setup NFS Server on CentOS 8 / RHEL 8 (linuxtechi.com)
$ sudo mkdir -p /exports/nfs1
$ sudo chown -R nobody: /exports/nfs1
$ sudo chmod -R 777 /exports/nfs1
$ sudo systemctl restart nfs-utils.service


Watching DNSmasq journal (logs)
$ journalctl -flu dnsmasq


General Networking & Stuff
$ host xxx.yyy.com
$ host -t PTR 172.23.14.28
$ ip a show ensp0n1s0f2
$ free (show free memory info)
$ lscpu | grep CPU


